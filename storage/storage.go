package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/store-service-temp/storage/postgres"
	"gitlab.com/store-service-temp/storage/repo"
)

type IsStorage interface {
	Store() repo.StoreStorageI
}

type StoragePg struct {
	Db          *sqlx.DB
	ProductRepo repo.StoreStorageI
}

func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:          db,
		ProductRepo: postgres.NewProductRepo(db),
	}
}

func (s StoragePg) Store() repo.StoreStorageI {
	return s.ProductRepo
}
