package postgres

import (
	"github.com/jmoiron/sqlx"
	pb "gitlab.com/store-service-temp/genproto/store"
)

type StoreRepo struct {
	Db *sqlx.DB
}

func NewProductRepo(db *sqlx.DB) *StoreRepo {
	return &StoreRepo{
		Db: db,
	}
}

func (s *StoreRepo) CreateAddresses(req *pb.Addresses) (*pb.Addresses, error) {
	l := len(req.Addresses)
	for i := 0; i < l; i++ {
		a := req.Addresses[i]
		err := s.Db.QueryRow(`INSERT INTO addresses (district, street, postcode) 
		values($1, $2, $3) returning id`, a.District, a.Street, a.Postcode).Scan(&req.Addresses[i].Id)
		if err != nil {
			return &pb.Addresses{}, err
		}
	}
	return req, nil
}

func (s *StoreRepo) GetAddressesById(req *pb.Id) (*pb.Address, error) {
	res := &pb.Address{}
	err := s.Db.QueryRow(`SELECT id, district, street, postcode from addresses where id = $1`, req.Id).Scan(
		&res.Id, &res.District, &res.Street, &res.Postcode,
	)
	return res, err
}

func (s *StoreRepo) UpdateAddress(req *pb.Address) error {
	_, err := s.Db.Exec(`UPDATE addresses SET district=$1, street=$2, postcode=$3 where id = $4`,
		req.District, req.Street, req.Postcode, req.Id)
	return err
}

func (s *StoreRepo) DeleteAddressById(req *pb.Id) error {
	_, err := s.Db.Exec(`DELETE FROM addresses where id = $1`, req.Id)
	return err
}
