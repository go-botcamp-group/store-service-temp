package postgres

import (
	pb "gitlab.com/store-service-temp/genproto/store"
)

func (s *StoreRepo) AddProductToStore(req *pb.StoresProduct) (pb.StoreProductInfo, int64, error) {
	_, err := s.Db.Exec(`insert into stores_products(store_id, product_id, amount, price)
	values($1, $2, $3, $4)`, req.StoreId, req.ProductId, req.Amount, req.Price)
	if err != nil {
		return pb.StoreProductInfo{}, 0, err
	}
	response := pb.StoreProductInfo{}
	response.Store, err = s.GetStoreById(&pb.Id{Id: req.StoreId})
	if err != nil {
		return pb.StoreProductInfo{}, 0, err
	}
	prdoductInfo := &pb.ProductInfo{}
	prdoductInfo.Amount = req.Amount
	prdoductInfo.Price = req.Price
	response.Product = prdoductInfo
	return response, req.ProductId, nil
}
