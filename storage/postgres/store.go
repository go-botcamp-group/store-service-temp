package postgres

import (
	pb "gitlab.com/store-service-temp/genproto/store"
)

func (s *StoreRepo) CreateStore(req *pb.Store) (*pb.StoreInfo, error) {
	response := &pb.StoreInfo{}
	err := s.Db.QueryRow(`insert into stores(name, address_id) values($1, $2) returning id, name`,
		req.Name, req.AddressId).Scan(&response.Id, &response.Name)
	if err != nil {
		return &pb.StoreInfo{}, err
	}
	temp := &pb.Id{Id: req.AddressId}
	response.Address, err = s.GetAddressesById(temp)
	return response, err
}

func (s *StoreRepo) GetStoreById(req *pb.Id) (*pb.StoreInfo, error) {
	response := &pb.StoreInfo{}
	err := s.Db.QueryRow(`SELECT id, name, address_id from stores where id = $1`, req.Id).Scan(
		&response.Id, &response.Name, &req.Id,
	)
	if err != nil {
		return &pb.StoreInfo{}, err
	}
	response.Address, err = s.GetAddressesById(req)
	return response, err
}
