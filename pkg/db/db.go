package db

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/store-service-temp/config"
	_ "github.com/lib/pq"
)

func ConnectToDb(cfg config.Config) (*sqlx.DB, error) {
	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase,
	)
	return sqlx.Connect("postgres", psqlString)
}
