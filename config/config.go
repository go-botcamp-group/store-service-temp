package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment        string
	PostgresHost       string
	PostgresPort       int
	PostgresDatabase   string
	PostgresUser       string
	PostgresPassword   string
	LogLevel           string
	GRPCPort           string
	ProductServiceHost string
	ProductServicePort int
}

func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", "5432"))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "store_service"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "Azizbek"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "azizbek"))

	c.ProductServiceHost = cast.ToString(GetOrReturnDefault("P_S_HOST", "localhost"))
	c.ProductServicePort = cast.ToInt(GetOrReturnDefault("P_S_PORT", 9000))

	c.GRPCPort = cast.ToString(GetOrReturnDefault("GRPC_PORT", ":8000"))

	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
