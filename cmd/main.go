package main

import (
	"net"

	"gitlab.com/store-service-temp/config"
	pb "gitlab.com/store-service-temp/genproto/store"
	"gitlab.com/store-service-temp/pkg/db"
	"gitlab.com/store-service-temp/pkg/logger"
	"gitlab.com/store-service-temp/service"
	grpcclient "gitlab.com/store-service-temp/service/grpcClient"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "store-service-temp")
	defer logger.CleanUp(log)

	log.Info("main: sqlx Config",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)

	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database: %v", logger.Error(err))
	}
	prClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Error while connection to product client", logger.Any("", err))
	}
	storeService := service.NewStoreService(prClient, connDb, log)

	lis, err := net.Listen("tcp", cfg.GRPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterStoreServiceServer(s, storeService)
	log.Info("Server is running", logger.String("Port", cfg.GRPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
