package grpcclient

import (
	"fmt"

	"gitlab.com/store-service-temp/config"
	pbd "gitlab.com/store-service-temp/genproto/product"
	"google.golang.org/grpc"
)

type ServiceManager struct {
	config         config.Config
	productService pbd.ProductServiceClient
}

func New(c config.Config) (*ServiceManager, error) {
	connection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.ProductServiceHost, c.ProductServicePort),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, fmt.Errorf("error while dial product service: host: %s and port: %d",
			c.ProductServiceHost, c.ProductServicePort)
	}
	serviceManager := &ServiceManager{
		config:         c,
		productService: pbd.NewProductServiceClient(connection),
	}

	return serviceManager, nil
}

func (s *ServiceManager) ProductService() pbd.ProductServiceClient {
	return s.productService
}
