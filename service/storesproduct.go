package service

import (
	"context"

	pbp "gitlab.com/store-service-temp/genproto/product"
	pb "gitlab.com/store-service-temp/genproto/store"
	"gitlab.com/store-service-temp/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *StoreService) AddProductToStore(ctx context.Context, req *pb.StoresProduct) (*pb.StoreProductInfo, error) {
	res, p_id, err := s.Storage.Store().AddProductToStore(req)
	if err != nil {
		s.Logger.Error("Error while adding product to store", logger.Any("insert", err))
		return &pb.StoreProductInfo{}, status.Error(codes.Internal, "Please check your data")
	}

	products, err := s.Client.ProductService().GetProductsByIds(ctx, &pbp.GetProductsByIdsRequest{Ids: []int64{p_id}})
	if err != nil {
		return &pb.StoreProductInfo{}, err
	}
	p := products.Products[0]
	tempC := &pb.Category{
		Id:   p.Category.Id,
		Name: p.Category.Name,
	}
	tempT := &pb.Type{
		Id:   p.Type.Id,
		Name: p.Type.Name,
	}
	res.Product.Id = p.Id
	res.Product.Category = tempC
	res.Product.Type = tempT
	res.Product.Model = p.Model
	res.Product.Name = p.Name

	return &res, nil
}
