package service

import (
	"context"

	pb "gitlab.com/store-service-temp/genproto/store"
	"gitlab.com/store-service-temp/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *StoreService) CreateStore(ctx context.Context, req *pb.Store) (*pb.StoreInfo, error) {
	res, err := s.Storage.Store().CreateStore(req)
	if err != nil {
		s.Logger.Error("Error while creating store", logger.Any("insert", err))
		return &pb.StoreInfo{}, status.Error(codes.Internal, "Please recheck store data")
	}

	
	return res, nil
}

func (s *StoreService) CreateStores(c context.Context, req *pb.PostStores) (*pb.GetStores, error) {
	res := &pb.GetStores{}
	for _, store := range req.Stores {
		resStore, err := s.Storage.Store().CreateStore(store)
		if err != nil {
			s.Logger.Error("Error while creating stores", logger.Any("insert", err))
			return &pb.GetStores{}, status.Error(codes.Internal, "Please recheck stores data")
		}
		res.Stores = append(res.Stores, resStore)
	}
	return res, nil
}

func (s *StoreService) GetStoreById(c context.Context, req *pb.Id) (*pb.StoreInfo, error) {
	res, err := s.Storage.Store().GetStoreById(req)
	if err != nil {
		s.Logger.Error("Error while getting storeinfo", logger.Any("get", err))
		return res, status.Error(codes.Internal, "Not found")
	}
	return res, nil
}

func (s *StoreService) GetStoresByIds(c context.Context, req *pb.Ids) (*pb.GetStores, error) {
	response := &pb.GetStores{}
	for _, id := range req.Ids {
		res, err := s.Storage.Store().GetStoreById(&pb.Id{Id: id})
		if err != nil {
			s.Logger.Error("Error while getting storeinfo", logger.Any("get", err))
			return response, status.Error(codes.Internal, "Not found")
		}
		response.Stores = append(response.Stores, res)
	}
	return response, nil
}
