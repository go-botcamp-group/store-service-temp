package service

import (
	"context"

	"github.com/jmoiron/sqlx"
	pb "gitlab.com/store-service-temp/genproto/store"
	"gitlab.com/store-service-temp/pkg/logger"
	grpcclient "gitlab.com/store-service-temp/service/grpcClient"
	"gitlab.com/store-service-temp/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type StoreService struct {
	Client  *grpcclient.ServiceManager
	Storage storage.IsStorage
	Logger  logger.Logger
}

func NewStoreService(client *grpcclient.ServiceManager, db *sqlx.DB, l logger.Logger) *StoreService {
	return &StoreService{
		Client:  client,
		Storage: storage.NewStoragePg(db),
		Logger:  l,
	}
}

func (s *StoreService) CreateAddresses(ctx context.Context, req *pb.Addresses) (*pb.Addresses, error) {
	res, err := s.Storage.Store().CreateAddresses(req)
	if err != nil {
		s.Logger.Error("Error while creatingAddresses", logger.Any("insert", err))
		return &pb.Addresses{}, status.Error(codes.Internal, "Please recheck address info")
	}
	return res, nil
}

func (s *StoreService) GetAddressesById(ctx context.Context, req *pb.Id) (*pb.Address, error) {
	res, err := s.Storage.Store().GetAddressesById(req)
	if err != nil {
		s.Logger.Error("Error while getting address by id", logger.Any("Get", err))
		return &pb.Address{}, status.Error(codes.NotFound, "Not Found")
	}
	return res, nil
}

func (s *StoreService) GetAddressesByIds(ctx context.Context, req *pb.Ids) (*pb.Addresses, error) {
	res := &pb.Addresses{}
	for _, id := range req.Ids {
		address, err := s.Storage.Store().GetAddressesById(&pb.Id{Id: id})
		if err != nil {
			s.Logger.Error("Error while Getting  address", logger.Any("get", err))
			return &pb.Addresses{}, status.Error(codes.Internal, "Couldn't get the data")
		}
		res.Addresses = append(res.Addresses, address)
	}
	return res, nil
}

func (s *StoreService) UpdateAddress(ctx context.Context, req *pb.Address) (*pb.Empty, error) {
	err := s.Storage.Store().UpdateAddress(req)
	if err != nil {
		s.Logger.Error("Error while Updating address", logger.Any("update", err))
		return &pb.Empty{}, status.Error(codes.Internal, "Couldn't Update the data")
	}
	return &pb.Empty{}, nil
}

func (s *StoreService) DeleteAddressById(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	err := s.Storage.Store().DeleteAddressById(req)
	if err != nil {
		s.Logger.Error("Error while deleting address", logger.Any("delete", err))
		return &pb.Empty{}, status.Error(codes.Internal, "Couldn't delete the data")
	}
	return &pb.Empty{}, nil
}
